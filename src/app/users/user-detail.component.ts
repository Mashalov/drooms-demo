import { OnInit, Component, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "@app/users/user.service";
import { User } from "@app/users/user.model";
import { RepoInfo } from "@app/users/user-repo-info.model";

@Component({
    selector: 'user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
    id: any;
    subscription: Subscription;
    user: User;
    repos: RepoInfo[];
    cols: any[];
    page = 0;
        //pagination will be implemented in later stage
    pageSize = 200;
    
    constructor(
        private route: ActivatedRoute,
        private userService:UserService,
        private router: Router) {
    }
    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['login']);
        });
        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'url', header: 'Url' },
            { field: 'description', header: 'Description' },
         ];
    }

    load(login: any) {
        this.userService.find(login).subscribe(user => {
            this.user = user.body;
            this.loadRepos(this.user.login);
        });
    }
    loadRepos(userLogin:string) {
        this.userService.getRepos(userLogin,this.page,this.pageSize).subscribe(repos => {
            this.repos = repos;
        });
    }
    
    ngOnDestroy(): void {
    }
     

}