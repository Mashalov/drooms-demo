import { Component, OnInit, OnDestroy } from "@angular/core";
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { User } from "@app/users/user.model";
import { UserService } from "@app/users/user.service";

 

@Component({
    selector: 'user-component',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

    users: User[];

    constructor(private userService: UserService) { 
    }

    ngOnInit(): void {
        this.loadAll();
    }
    
    loadAll() {
        this.userService
        .loadAll()
        .subscribe(
            (data) => this.onSuccess(data),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    private onSuccess(data: User[]) {
        this.users = data;
    }

    private onError(error: string) {
        console.log(error);
    }

    ngOnDestroy(): void {
      
    }
}
