import { NgModule } from '@angular/core';
import { UserComponent } from '@app/users/user.componet';
import { UserDetailComponent } from '@app/users/user-detail.component';
import { UserService } from '@app/users/user.service';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UserRoutingModule } from '@app/users/user.route';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        UserRoutingModule
    ],
    declarations: [
        UserComponent,
         UserDetailComponent,
    ],
    entryComponents: [UserComponent],
    providers: [UserService],
    exports: [UserComponent]
})
export class UserModule {}
