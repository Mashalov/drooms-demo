import { UserDetailComponent } from "@app/users/user-detail.component";
import { Routes, RouterModule } from "@angular/router";
import { Shell } from "@app/shell/shell.service";
import { AboutComponent } from "@app/about/about.component";
import { extract } from "@app/core";
import { NgModule } from "@angular/core";

 
 
const routes: Routes = [
    Shell.childRoutes([
      { path: 'user/:login', component: UserDetailComponent }
    ])
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
  })
  export class UserRoutingModule { }
  