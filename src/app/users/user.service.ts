import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient, HttpParams } from '@angular/common/http';
import { User } from '@app/users/user.model';
import { Observable } from 'rxjs';
import { RepoInfo } from '@app/users/user-repo-info.model';
export type EntityResponseType = HttpResponse<User>;
@Injectable()
export class UserService {
 
    private resourceUrl = 'https://api.github.com/users';
    private resourceReposUrl = 'https://api.github.com/users';

    constructor(private http: HttpClient) { }
    
    loadAll() {
        return this.http
            .get<User[]>(this.resourceUrl);
    }
    find(login: string): Observable<EntityResponseType> {
        return this.http.get<User>(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }
    //to be refactored and added in repository service
    getRepos(userLogin: any, page: any, pageSize: any) {
        let options=new HttpParams();;
        options = options.set('page', page);
        options = options.set('per_page', pageSize);
         return this.http.get<RepoInfo[]>(`${this.resourceReposUrl}/${userLogin}`+"/repos",{params:options});
    }
   
   
}
